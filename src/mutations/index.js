import gql from 'graphql-tag';


const RegisterMutation = gql`
    mutation Register(
        $firstName: String, 
        $lastName: String, 
        $sex: String, 
        $email: String, 
        $password: String,
        $userName: String,
        $accountType: String) {
        createUser(
            firstName: $firstName, 
            lastName: $lastName, 
            sex: $sex, 
            accountType: $accountType, 
            email: $email, 
            userName: $userName,
            password: $password) {
                status
                message
                accessToken
            }
        }`



const LoginMutation = gql`
    mutation Login($field: String, $password: String) {
        login(field: $field, password: $password) { 
            accessToken
            message
            status
            user {
                _id
                firstName
                lastName
                email
                accountType
                avatar
            }
        }
    }`

const VerifyMutation = gql`
    mutation Verify($id: ID!) {
        verifyUser(id: $id) {
            message
            status
            user {
                _id
                verified
            }
        }
    }
`

const UpdateUserMutation = gql`
    mutation Update($id: ID!, $firstName: String, 
        $lastName: String, 
        $userName: String, 
        $address: String, 
        $postalCode: String, 
        $age: String, 
        $phoneNumber: String, 
        $verified: Boolean, 
        $email: String, 
        $sex: String,
        $rating: String, 
        $avatar: String, 
        $accountType: String, 
        $token: String) {
        updateUser(id: $id, 
            firstName: $firstName, 
            lastName: $lastName,
            userName: $userName, 
            address: $address, 
            postalCode: $postalCode, 
            age: $age, 
            phoneNumber: $phoneNumber, 
            verified: $verified, 
            email: $email, 
            sex: $sex, 
            rating: $rating, 
            avatar: $avatar, 
            accountType: $accountType, 
            token: $token) { 
            status
            message
            user {
                _id
                firstName
            }
        }
    }`


const UpdateTransactionMutation = gql`
    mutation UpdateTransaction($id: ID!, $cardToken: JSON, 
        $bankAccountToken: JSON, 
        $clientIp: String,
        $created: String,
        $customerId: String,
        $balance: Int){
        updateTransaction(userId: $id, 
            cardTokens: $cardToken,
            created: $created,
            clientIp: $clientIp
            bankAccounts: $bankAccountToken, 
            customerId: $customerId, 
            balance: $balance) {
            message
            status
        }
    }`

const UpdateMandatoryFields = gql `
mutation UpdateFields($userId: ID!, $accountId: ID!
    $dob: String,
    $city: String,
    $state: String,
    $postalCode: String,
    $address: String,
    $socialInsuranceNumber: String){
        updateMandatoryFields(userId: $userId,
            accountId: $accountId, 
            socialInsuranceNumber: 
            $socialInsuranceNumber, 
            dob: $dob,
            city: $city,
            state: $state,
            postalCode: $postalCode,
            address: $address) {
        message
        status
    }
}`

const DispatchRequest = gql`
    mutation Dispatch(
        $userId: ID!, 
        $title: String, 
        $content: String, 
        $audience: String, 
        $amountRequested: Int, 
        $settlementDate: String) {
            dispatchRequest(
                userId: $userId, 
                title: $title, 
                content: $content, 
                audience: $audience, 
                amountRequested: $amountRequested, 
                settlementDate: $settlementDate) {
                status
                message
            }
    }`


export { RegisterMutation, LoginMutation, UpdateUserMutation, VerifyMutation, UpdateTransactionMutation, UpdateMandatoryFields, DispatchRequest }