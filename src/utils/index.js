import { isEmpty, isEqual } from 'lodash'
import validator from 'email-validator'
import jwtDecode from 'jwt-decode'

const validate = (steps) => {
    switch (steps.title) {
        case 'Basic Info':
            return validateBasicInfo(steps)
        case 'Account Type':
            return validateAccountType(steps)
        case 'Terms And Conditions':
            return validateTermsAndConditions(steps)
        default:
            return
    }
}

const validateBasicInfo = ({ store }) => {
    store = store.getState()
    let isValid = true
    for (let item in store) {
        if (isEmpty(store[item])) {
            return { isValid: false, errorMessage: `${item.charAt(0).toUpperCase() + item.slice(1)} field is either empty or invalid` }
        }
    }
    isValid = validator.validate(store.email)
    return (isValid) ? doesPasswordsMatch(store) : { isValid, errorMessage: `email address ${store.email} is invalid` }
}

const doesPasswordsMatch = ({ password, confirmPassword }) => {
    return (isEqual(password, confirmPassword)) ? { isValid: true } : { isValid: false, errorMessage: `password and confirm password do not match` }
}

const validateAccountType = ({ store }) => {
    store = store.getState()
    const { result } = store
    return (!isEmpty(result) && (isEqual(result, 'loaner') || isEqual(result, 'borrower'))) ? { isValid: true } : { isValid: false, errorMessage: `you have to be either a borrower or a loaner` }
}

const validateTermsAndConditions = ({ store }) => {
    store = store.getState()
    return (isEqual(store.checked, true)) ? { isValid: true } : { isValid: false, errorMessage: `please read and check the pocketloan terms and conditions` }
}

const getUser = () => {
    const accessToken = localStorage.getItem('accessToken')
    if (accessToken) {
        try {
            return jwtDecode(accessToken)
        } catch (err) {
            console.log(err)
        }
    }
    return {}
}

const forceHttps = () => {
    if (window.location.protocol !== 'https:' && window.location.hostname !== 'localhost') {
        window.location.href = 'https:' + window.location.href.substring(window.location.protocol.length)
    }
}

const getFirebaseConfig = () => {
    return {
        apiKey: "AIzaSyBVP3Q4Rd-90sLJVJXqiTMIR1NjyCOOZA0",
        authDomain: "pocketloan-storage.firebaseapp.com",
        databaseURL: "https://pocketloan-storage.firebaseio.com",
        projectId: "pocketloan-storage",
        storageBucket: "pocketloan-storage.appspot.com",
        messagingSenderId: "18386962252"
    }
}

const isDevelopment = process.env.NODE_ENV === 'development'

export { validate, getUser, forceHttps, getFirebaseConfig, isDevelopment }