import { Store, connectDevTools } from 'simple-react-store'

const basicInfoStore = new Store({
    firstName: '',
    lastName: '',
    email: '',
    sex: 'Other',
    password: '',
    confirmPassword: ''
})

const accountTypeStore = new Store({
    loanerType: 'default',
    borrowerType: 'primary',
    result: ''
})

const termsAndConditionStore = new Store({
    checked: false
})

const userStore = new Store({})

connectDevTools(userStore)

export { termsAndConditionStore, accountTypeStore, basicInfoStore, userStore }