import React, { Component } from 'react'
import Img from 'react-image'
import { Layout, Menu } from 'antd'
import { css } from 'emotion'
const { Header, Content, Footer } = Layout

const textWhite = css`
    color: white;
`
const textBlue = css`
    color: #1073BA;
`

class FormLayout extends Component {
    constructor(props) {
        super(props)
        this.state = props
    }

    render() {
        const { content } = this.state
        return (
            <Layout className="layout">
                <Header>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['2']}
                        style={{ lineHeight: '64px' }}>
                    </Menu>
                    <Img height="45px" width="45px" src={`/favicon.ico`} alt="logo" />
                    <b className={textWhite}>OCKET</b><b className={textBlue}>LOAN</b>
                </Header>
                <Content style={{ padding: '0 50px', marginTop: '2%' }}>
                    <div style={{ background: '#fff', padding: 24, minHeight: '77vh' }}>
                        {content()}
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    <b> Copyright ©2018  PocketLoan ltd. </b>
                </Footer>
            </Layout>
        )
    }
}

export { FormLayout }