import React, { Component } from 'react'
import { isEmpty } from 'lodash'
import { Layout, Menu, Icon, Avatar, Badge, notification } from 'antd'
import { css } from 'emotion'
import io from 'socket.io-client'
import { Switch, Route, Link } from 'react-router-dom'
import { Widget, addResponseMessage } from 'react-chat-widget'
import { userStore } from '../../store'
import 'react-chat-widget/lib/styles.css'
import { getUser, isDevelopment } from '../../utils'

const { Header, Sider, Content, Footer } = Layout
const socketIoEndpoint = isDevelopment ? 'http://localhost:3002' : ''
const chat = io(`${socketIoEndpoint}/chat?token=${localStorage.accessToken}`)
const notify = io(`${socketIoEndpoint}/notify?token=${localStorage.accessToken}`, { forceNew: true })

const trigger = css`
    font-size: 18px;
    line-height: 64px;
    padding: 0 24px;
    cursor: pointer;
    transition: color .3s;
    :hover {
      color: #1890ff;
    }
`

const logo = css`
    height: 32px;
    background: rgba(255,255,255,.2);
    margin: 16px;
`

const fitToScreen = css`
    width: auto;
    height: auto;
    zoom: 100%;
}
`

class DashboardLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            collapsed: true,
            loading: true,
            user: {},
            chat,
            notify,
            badgeCount: null,
            customerServiceMessage: null,
            previousMessage: '',
            notificationCount: 0,
            dispatchResponse: {}
        }
        this.LogOut = this.LogOut.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleNewUserMessage = this.handleNewUserMessage.bind(this)
    }

    componentDidMount() {
        this.props.data.refetch()
        this.state.chat.once('connected', (data) => {
            const { message, badgeCount } = data
            addResponseMessage(message)
            this.setState({ badgeCount })
        })
    }

    componentDidUpdate() {
        const { user } = this.state
        this.state.chat.on('customer_service', (data) => {
            if (!isEmpty(data)) {
                const { userId, text } = data
                const response = text.split(':')
                if (userId === getUser().userId && this.state.previousMessage !== text && response.length === 2 && response[0] === user.firstName) {
                    this.setState({ previousMessage: text }, () => {
                        this.setState({ badgeCount: 2 }, () => {
                            addResponseMessage(response[1])
                        })
                    })
                }
            }
        })
        this.state.notify.on('increment', (data) => {
            if (user.accountType === data.audience && !isEmpty(data.request)) {
                this.setState({ dispatchResponse: data }, () => {
                    this.setState({ notificationCount: this.state.notificationCount + 1 }, () => {
                        const { title, content } = data.request
                        this.openNotification(title, content)
                    })
                })
            }
        })
    }

    componentWillReceiveProps(nextProps) {
        const { loading, user } = nextProps.data
        this.setState({ loading, user }, () => {
            userStore.setState(this.state.user)
        })
    }

    handleNewUserMessage = (newMessage) => {
        // console.log(`New message incomig! ${newMessage}`);
        // Now send the message throught the backend API
        const message = `${this.state.user.firstName}: ${newMessage}`
        this.state.chat.emit('newMessage', { message })
        // addResponseMessage('This is a custom reply..')
    }

    toggle = () => {
        this.setState({ collapsed: !this.state.collapsed })
    }

    LogOut() {
        localStorage.removeItem('accessToken')
        this.props.history.push('/login')
    }

    headerLayout = () => {
        const width = window.innerWidth
        if (width <= 340) {
            return '15%'
        }
        if (width > 340 && width <= 380) {
            return '28%'
        }
        if (width >= 400 && width <= 1120) {
            return '54%'
        }
        return '78%'
    }

    activeNav() {
        switch (this.props.location.pathname) {
            case '/dashboard':
                return '1'
            case '/dashboard/profile':
                return '2'
            case '/dashboard/inbox':
                return '3'
            case '/dashboard/transactions':
                return '4'
            case '/dashboard/accounts':
                return '5'
            case '/dashboard/settings':
                return '6'
            case '/dashboard/help':
                return '7'
            default:
                return '1'
        }
    }

    handleClick = ({ key }) => {
        const prefix = '/dashboard'
        key = Number(key)

        switch (key) {
            case 1:
                this.props.history.push(`${prefix}`)
                break
            case 2:
                this.props.history.push(`${prefix}/profile`)
                break
            case 3:
                this.props.history.push(`${prefix}/inbox`)
                break
            case 4:
                this.props.history.push(`${prefix}/transactions`)
                break
            case 5:
                this.props.history.push(`${prefix}/accounts`)
                break
            case 6:
                this.props.history.push(`${prefix}/settings`)
                break
            case 7:
                this.props.history.push(`${prefix}/help`)
                break
            default:
                this.props.history.push(`${prefix}`)
                break
        }

    }

    buildLogoText = () => {
        return `
            <p style="margin-right: 12%;"><b style="color: white;">POCKET</b><b style="color: #1073BA;">LOAN</b></p>
        `
    }

    insertLogoText = ({ collapsed }) => {
        const div = document.getElementsByClassName('css-1xj0ho2')
        if (!isEmpty(div[0])) {
            if (!collapsed) {
                div[0].innerHTML = this.buildLogoText()
            } else {
                div[0].innerHTML = `<img src="/favicon.ico" width="35px" height="35px" />`
            }
        }
    }

    openNotification = (message, description) => {
        notification.open({ message, description })
    }

    welcomeBackMessage(user) {
        if (user) {
            if (!localStorage.getItem('isNotified')) {
                this.openNotification('Welcome Back', `Hi ${user.firstName} welcome back to pocketloan`)
                localStorage.setItem('isNotified', true)
            }
        }
    }

    render() {
        const { Home, Profile, Inbox, TransactionHistory, Accounts, Settings, HelpAndSupport, Accept, Request } = this.props.components
        const { user } = this.state
        this.insertLogoText(this.state)
        return (
            <Layout className={fitToScreen}>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}>
                    <div className={logo} />
                    <Menu onClick={this.handleClick} theme="dark" mode="inline" defaultSelectedKeys={[this.activeNav()]} title="SubMenu">
                        <Menu.Item key="1">
                            <Icon type="home" />
                            <span>Home</span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="user" />
                            <span>Profile</span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="inbox" />
                            <span>Inbox</span>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Icon type="area-chart" />
                            <span>Transaction History</span>
                        </Menu.Item>
                        <Menu.Item key="5">
                            <Icon type="profile" />
                            <span>Accounts</span>
                        </Menu.Item>
                        <Menu.Item key="6">
                            <Icon type="setting" />
                            <span>Settings</span>
                        </Menu.Item>
                        <Menu.Item key="7">
                            <Icon type="usergroup-add" />
                            <span>Help And Support</span>
                        </Menu.Item>
                        <Menu.Item onClick={this.LogOut} key="8">
                            <Icon type="logout" />
                            <span>Log Out</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        {window.innerWidth > 760 && <Icon
                            className={trigger}
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />}
                        <span style={{ float: 'right' }}>
                            <Link to={`/dashboard/inbox`}>
                                <Badge count={this.state.notificationCount}>
                                    <Icon style={{ marginLeft: window.innerWidth <= 767 ? '-30vw' : '-12vw', fontSize: 17 }} type="bell" />
                                </Badge>
                            </Link>
                            <Link to={`/dashboard/profile`}>
                                <Avatar style={{ marginLeft: window.innerWidth <= 767 ? '-16vw' : '-6vw' }} size="large" src={(user) ? user.avatar : ''} />
                            </Link>
                        </span>
                    </Header>
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div className="container-fluid" style={{ padding: 24, background: '#fff', minHeight: '78vh' }}>
                            <Widget
                                handleNewUserMessage={this.handleNewUserMessage}
                                profileAvatar={`https://avatars2.githubusercontent.com/u/38562472?s=60&v=4`}
                                title="Pocketloan Chat"
                                subtitle={user ? user.userName : ''}
                                badge={this.state.badgeCount}
                                autofocus={false}
                            />
                            <Switch>
                                <Route exact path='/' render={() => (<Home {...this.props} data={this.props.data} />)} />
                                <Route exact path='/dashboard' render={() => (<Home {...this.props} data={this.props.data} />)} />
                                <Route path='/dashboard/profile' render={() => (<Profile {...this.props} data={this.props.data} />)} />
                                <Route path='/dashboard/inbox' {...this.props} component={Inbox} />
                                <Route path='/dashboard/transactions' {...this.props} component={TransactionHistory} />
                                <Route path='/dashboard/accounts' {...this.props} component={Accounts} />
                                <Route path='/dashboard/settings' {...this.props} component={Settings} />
                                <Route path='/dashboard/help' {...this.props} component={HelpAndSupport} />
                                <Route path='/dashboard/request' {...this.props} component={Request} />
                                <Route path='/dashboard/accept/:id' {...this.props} component={Accept} />
                            </Switch>
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        <b> Copyright ©2018  PocketLoan ltd. </b>
                    </Footer>
                </Layout>
            </Layout>
        )
    }
}

export { DashboardLayout }