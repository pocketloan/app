import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import { isEmpty } from 'lodash'
import { getUser } from '../../../../utils'
import { SettingsQuery } from '../../../../queries'
import { Button, Switch, Divider, Checkbox, Input, Card } from 'antd'
const CheckboxGroup = Checkbox.Group

class UserSettings extends Component {
    onChange(checked) {
        console.log(`switch to ${checked}`)
    }
    onCheck(checkedValues) {
        console.log('checked = ', checkedValues)
    }
    render() {
        const plainOptions = ['On', 'Off']
        const transaction = (this.props.data.user) ? this.props.data.user.transaction : {}
        const card = (transaction.cardTokens && transaction.cardTokens.length > 0) ? transaction.cardTokens[transaction.cardTokens.length - 1].cardToken.card : {}
        return (
            <div className="container-fluid">
                <div className="text-center">
                    <Button disabled={true} type="primary" icon="setting" size="large">Settings</Button>
                    <br />
                </div>
                <Divider orientation="left">Status</Divider>
                <Card>
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}> Account Status: </b>  <Switch style={{ marginTop: '6px', marginLeft: '1px' }} defaultChecked onChange={this.onChange} />
                    <br />
                    <br />
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}> Change Account: </b>
                    <Button style={{ marginTop: '5px', marginLeft: '3px' }} type="primary" icon="minus" size="large">Basic</Button>
                    <Button style={{ marginTop: '5px', marginLeft: '3px' }} disabled={true} type="primary" icon="plus" size="large">Lender</Button>
                </Card>
                <br />
                <Divider orientation="right">Extras</Divider>
                <Card loading={this.props.data.loading}>
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}> Postal Code: </b> <Input disabled={true} style={{ marginTop: '6px', width: (window.innerWidth <= 420) ? '150px' : '200px' }} placeholder={(!isEmpty(card)) ? card.address_zip : 'none'} />
                    <br /> <br />
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}> Primary Card: </b> <Input disabled={true} style={{ marginTop: '6px', width: (window.innerWidth <= 420) ? '150px' : '200px' }} placeholder={`**** **** **** ${(!isEmpty(card)) ? card.last4 : '****'}`} />
                    <br /> <br />
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}> Primary Email: </b> <Input disabled={true} style={{ marginTop: '6px', width: (window.innerWidth <= 420) ? '150px' : '200px' }} placeholder={(this.props.data.user) ? this.props.data.user.email : 'none'} />
                    <br /> <br />
                    <b style={{ marginTop: '6px', fontSize: '16px', marginLeft: '3px' }}>  Notifications: </b> <CheckboxGroup style={{ marginTop: '5px', marginLeft: '3px' }} options={plainOptions} defaultValue={['Off']} onChange={this.onCheck} />
                    <br /> <br />
                </Card>
                <br/>
            </div>
        )
    }
}

const Settings = graphql(SettingsQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserSettings)

export { Settings }