import React, { Component } from 'react'
import moment from 'moment'
import { List, Card, Avatar } from 'antd'
import { isEmpty } from 'lodash'
import { graphql } from 'react-apollo'
import { getUser } from '../../../../../utils'
import { UserActivityQuery } from '../../../../../queries'

class UserActivityLog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            activity: []
        }
    }

    componentDidMount() {
        this.props.data.refetch()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ data: nextProps.data })
    }
    
    last(arr, limit) {
        if (!isEmpty(arr)) {
            return arr.slice(Math.max(arr.length - limit))
        }
        return []
    }

    evalContent(content) {
        if(/\d/.test(content)) {
            content = content.split(' ')
            content[content.length - 1] = moment(new Date(content[content.length - 1])).format('lll')
            content = content.join(' ')
            return content
        }
        return content
    }

    render() {
        const { avatar } = this.props
        const { user, loading } = this.state.data
        const activity = (user) ? user.activity : {}
        const userActivity = (!isEmpty(activity) && typeof activity.userActivity !== 'undefined') ? activity.userActivity : []
        return (<Card className="card" title="Activity Log" loading={loading}>
            <List
                itemLayout="horizontal"
                dataSource={this.last(userActivity, 6)}
                renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar src={avatar} />}
                            title={item.title}
                            description={this.evalContent(item.content)}
                        />
                    </List.Item>
                )}
            />
        </Card>)
    }
}

const ActivityLog = graphql(UserActivityQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserActivityLog)

export { ActivityLog }