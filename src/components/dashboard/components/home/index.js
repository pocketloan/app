import React, { Component } from 'react'
import RC2 from 'react-chartjs2'
import { Card, Rate, Alert, Progress, Button } from 'antd'
import { ActivityLog } from './components'
import { Inbox } from '../../components/inbox'

const graphData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'Transactions',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40],
        }
    ]
}

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data
        }
        this.borrowerRequest = this.borrowerRequest.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ data: nextProps.data })
    }

    onPanelChange(value, mode) {
        // console.log(value, mode);
    }

    isLoaner(user) {
        if (user) {
            return (user.accountType === 'loaner') ? true : false
        }
        return false
    }

    borrowerRequest() {
        this.props.history.push(`/dashboard/request`)
    }

    displayValue(data, field) {
        return (data) ? data[field] : ''
    }

    renderRequest(user, loading) {
        if (user) {
            return (user.accountType === 'borrower') ? (<Card className="card" title="Send Requests" loading={loading} >
            <Alert style={{ fontWeight: 'bold', textAlign: 'center' }} message="Request for funds from loaners agreeing to thier terms" type="info" />
            <br />
            <Button onClick={this.borrowerRequest} icon="notification" type="primary" block>Request</Button>
            </Card>) : (<Card className="card" title="Requests" loading={loading} >
                <Inbox isHome={true} {...this.props}/>
            </Card>)
        }
    }

    render() {
        const { user, loading } = this.state.data
        return (
            <div className="card-columns">
                <Card className="card" title="Balance" loading={loading} bordered={true}>
                    <Alert style={{ fontWeight: 'bold', textAlign: 'center' }} message={`Your Current Balance Is $${(user) ? user.transaction.balance : ''}`} type="info" />
                </Card>
                <Card className="card" title="Rating" loading={loading}>
                    <Rate allowHalf defaultValue={5} disabled={true} />
                </Card>
                <ActivityLog avatar={this.displayValue(user, 'avatar')}/>
                {this.renderRequest(user, loading)}
                <Card className="card" title="Credit Score" loading={loading} >
                    <p>Coming Soon</p>
                </Card>
                {(window.innerWidth >= 1165 && this.isLoaner(user)) ? <Card className="card" title="Profit Checks" loading={loading} >
                    <div style={{ textAlign: 'center' }}>
                        <RC2 data={graphData} type='line' />
                    </div>
                </Card> : ''}
                <Card className="card" title="Calculator" loading={loading} >
                    <p>Coming Soon</p>
                </Card>
                <Card className="card" title="Payment Tracker" loading={loading} >
                    <div style={{ textAlign: 'center' }}>
                        <Progress type="circle" percent={75} format={() => `3 Hours`} />
                    </div>
                </Card>
            </div>
        )
    }
}

export { Home } 