import React, { Component } from 'react'
import { isEmpty } from 'lodash'
import { graphql } from 'react-apollo'
import { DispatchRequest } from '../../../../mutations'
import { getUser } from '../../../../utils'
import { Form, DatePicker, Button, Card, InputNumber, Divider, Checkbox, Input, Select, message } from 'antd'

const FormItem = Form.Item
const CheckboxGroup = Checkbox.Group
const Option = Select.Option

function validatePrimeNumber(number) {
    if (number >= 20 && number <= 500) {
        return {
            validateStatus: 'success',
            errorMsg: null,
        }
    }
    return {
        validateStatus: 'error',
        errorMsg: 'Amount should be between 20 to 500',
    }
}

class BorrowerRequest extends Component {
    constructor(props) {
        super(props)
        this.state = {
            number: {
                value: 20
            },
            options: ['Yes', 'No'],
            checked: 'No',
            settlementDate: '',
            loading: false
        }
        this.onChecked = this.onChecked.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.updateDate = this.updateDate.bind(this)
    }

    onChecked(checked) {
        this.setState({ checked: checked[1] })
    }

    handleSubmit() {
        this.setState({ loading: true }, () => {
            const { settlementDate, number: { value } } = this.state
            if (isEmpty(settlementDate) || !(value >= 20 && value <= 500)) {
                this.setState({ loading: false }, () => {
                    message.error('Empty or invalid fields. try again')
                })
            } else {
                const title = 'Funds Requested'
                const content = `A borrower just requested funds of $${value} to be paid back on ${settlementDate}`
                const args = { userId: getUser().userId, audience: 'loaner', amountRequested: value, settlementDate, title, content }
                this.props.mutate({ variables: args }).then((res) => {
                    this.setState({ loading: false }, () => {
                        if (res.data.dispatchRequest.status === 200) {
                            message.success(res.data.dispatchRequest.message)
                        } else {
                            if (res.data.dispatchRequest.status === 409) {
                                message.error(res.data.dispatchRequest.message)
                            } else {
                                message.error('Something went wrong on our side. Please try again')
                            }
                        }
                    })
                }).catch(err => { throw err })
            }
        })
    }

    updateDate(date, dateString) {
        this.setState({ settlementDate: dateString })
    }


    handleNumberChange = (value) => {
        this.setState({
            number: {
                ...validatePrimeNumber(value),
                value,
            },
        })
    }

    onSelect(value) {
        console.log(`selected ${value}`);
    }


    render() {
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        }
        const config = {
            rules: [{ type: 'object', required: true, message: 'Please select time!' }],
        }
        const number = this.state.number
        const tips = 'Max Amount is $500 for now'
        return (
            <div className={ (window.innerWidth >= 768) ? 'container': 'width-full' }>
                <Card style={{ width: '100%' }} title="Request Funds" bordered={false}>
                    <Card style={{ width: '100%' }} hoverable>
                        <div>
                            <FormItem
                                {...formItemLayout}
                                label="Date"
                                help={`Expected date for funds to be returned to the loaner after borrowing`}
                            >
                                {getFieldDecorator('date-picker', config)(
                                    <DatePicker onChange={this.updateDate} />
                                )}
                            </FormItem>
                            <FormItem
                                {...formItemLayout}
                                label="Amount"
                                validateStatus={number.validateStatus}
                                help={number.errorMsg || tips}
                            >
                                <InputNumber
                                    min={20}
                                    max={500}
                                    value={number.value}
                                    onChange={this.handleNumberChange}
                                />
                            </FormItem>
                            <Divider dashed />
                            <FormItem {...formItemLayout} label="Do you need a payment plan?">
                                <CheckboxGroup options={this.state.options} value={[this.state.checked]} onChange={this.onChecked} />
                                <br />
                                {
                                    this.state.checked === 'Yes' &&
                                    <div style={{ textAlign: 'left' }}>
                                        <Input style={{ width: '200px', margin: '0 8px 8px 0' }} placeholder="Enter Amount" />
                                        <Input style={{ width: '200px', margin: '0 8px 8px 0' }} placeholder="Choose date" />
                                        <Select defaultValue="Choose plan" style={{ width: '200px' }} onChange={this.onSelect}>
                                            <Option value="jack">Jack</Option>
                                            <Option value="lucy">Lucy</Option>
                                            <Option value="disabled" disabled>Disabled</Option>
                                            <Option value="Yiminghe">yiminghe</Option>
                                        </Select>
                                    </div>
                                }
                            </FormItem>
                            <FormItem
                                wrapperCol={{
                                    xs: { span: 24, offset: 0 },
                                    sm: { span: 16, offset: 8 },
                                }}
                            >
                                <Button icon="export" size="large" type="primary" onClick={this.handleSubmit} loading={this.state.loading}>Request</Button>
                            </FormItem>
                        </div>
                    </Card>
                </Card>
            </div>
        )
    }
}

const Request = graphql(DispatchRequest)(Form.create()(BorrowerRequest))

export { Request }