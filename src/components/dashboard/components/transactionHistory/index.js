import React, { Component } from 'react'
import moment from 'moment'
import { isEmpty } from 'lodash'
import { graphql } from 'react-apollo'
import { Button, Timeline, Icon } from 'antd'
import { getUser } from '../../../../utils'
import { TransactionHistoryQuery } from '../../../../queries'

class UserTransactionHistory extends Component {
    componentDidMount() {
        this.props.data.refetch()
    }

    evalContent(content) {
        if (/\d/.test(content)) {
            content = content.split(' ')
            content[content.length - 1] = moment(content[content.length - 1]).format('lll')
            content = content.join(' ')
            return content
        }
        return content
    }

    render() {
        const user = !isEmpty(this.props.data.user) ? this.props.data.user : {}
        const activity = !isEmpty(user.activity) ? user.activity : {}
        const { dateCreated, transactionActivity } = activity
        return (
            <div className="container-fluid text-center">
                <Button disabled={true} style={{ marginLeft: '10px', marginTop: '5px' }} type="default" icon="bar-chart" size='large'> Transaction History </Button>
                <br />
                <br />
                <div>
                    <Timeline pending={this.props.data.loading} reverse={true} mode="alternate">
                        {dateCreated && <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />}>Pocketloan Transaction Account Created on {this.evalContent(dateCreated)} </Timeline.Item>}
                        {(transactionActivity && transactionActivity.length > 0) && transactionActivity.map((transaction, index) => (<Timeline.Item key={index} color="green">{this.evalContent(transaction.content)}</Timeline.Item>))}
                        <Timeline.Item color="red">No Other Transaction History is available</Timeline.Item>
                    </Timeline>
                </div>
            </div>
        )
    }
}

const TransactionHistory = graphql(TransactionHistoryQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserTransactionHistory)

export { TransactionHistory }