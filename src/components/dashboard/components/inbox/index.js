import React, { Component } from 'react'
import moment from 'moment'
import { List, Avatar, Button, Spin } from 'antd'
import { graphql } from 'react-apollo'
import { RequestsQuery } from '../../../../queries'
import { getUser } from '../../../../utils'

class UserInbox extends Component {
  componentDidMount() {
    this.props.data.refetch()
  }

  evalContent(content) {
    if (/\d/.test(content)) {
      content = content.split(' ')
      content[content.length - 1] = moment(content[content.length - 1]).format('ll')
      content = content.join(' ')
      return content.replace('just', '')
    }
    return content
  }

  render() {
    return (
      <div className="container">
        {!this.props.isHome &&
          <div className="text-center">
            <Button disabled={true} style={{ marginLeft: '10px', marginTop: '5px' }} type="primary" icon="inbox" size='large'> Requests </Button>
            <br />
            <br />
          </div>
        }
        {this.props.data.loading ? (
          <div className="text-center">
            <Spin />
          </div>
        ) : <List
            itemLayout="horizontal"
            dataSource={this.props.data.user.activity.requests}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src={item.user.avatar} />}
                  title={item.title}
                  description={this.evalContent(item.content)}
                  onClick={() => this.props.history.push(`/dashboard/accept/${item._id}`)}
                  style={{ cursor: 'pointer' }}
                />
              </List.Item>
            )}
          />}
      </div>
    )
  }
}

const Inbox = graphql(RequestsQuery, {
  options: () => { return { variables: { id: getUser().userId } } }
})(UserInbox)

export { Inbox }