import React, { Component } from 'react'
import { Card, Alert, Button } from 'antd'
import { graphql } from 'react-apollo'
import { isEmpty } from 'lodash'
import { getUser } from '../../../../utils'
import { AcceptRequestQuery } from '../../../../queries'

class UserAccepts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            requestId: '',
            cardInfo: {},
            loading: false,
            request: {}
        }
        this.isAccountComplete = this.isAccountComplete.bind(this)
        this.handleAcceptRequest = this.handleAcceptRequest.bind(this)
    }

    componentWillMount() {
        this.props.data.refetch()
        this.setState({ requestId: this.props.match.params.id })
    }

    componentWillReceiveProps(nextProps) {
        const activity = !isEmpty(nextProps.data.user) ? nextProps.data.user.activity : {}
        if (!isEmpty(activity)) {
            activity.requests.forEach(request => {
                if (request._id === this.state.requestId) {
                    this.setState({ request })
                }
            })
            if (activity.transaction.cardTokens.length > 0) {
                this.setState({ cardInfo: activity.transaction.cardTokens[activity.transaction.cardTokens.length - 1].cardToken })
            }
        }
    }

    isAccountComplete(user) {
        let result = false
        const activity = !isEmpty(user) ? user.activity : {}
        if (!isEmpty(activity)) {
            if (activity.transaction.accounts.length < 1 || activity.transaction.cardTokens.length < 1 || isEmpty(activity.transaction.customerId)) {
                result = true
            }
        }
        return result
    }

    handleAcceptRequest() {
        this.setState({ loading: true })
    }

    render() {
        let amountRequested
        const activity = !isEmpty(this.props.data.user) ? this.props.data.user.activity : {}
        if (!isEmpty(activity)) {
            activity.requests.forEach(request => {
                if (request._id === this.state.requestId) {
                    amountRequested = request.amountRequested
                }
            })
        }
        return (
            <div className={(window.innerWidth >= 768) ? 'container' : 'width-full'}>
                <Card loading={this.props.data.loading} style={{ width: '100%' }} title="Accept Request" bordered={false}>
                    <Card className="text-center" style={{ width: '100%' }}>
                        <Alert message="Make sure you've added your bank account and card information" type="warning" />
                        <br />
                        {
                            !this.isAccountComplete(this.props.data.user) && <div>
                                <Alert message={`A charge of $${this.state.request.amountRequested || amountRequested} will be taken from your card once you click accept`} type="info" />
                                <br />
                            </div>
                        }
                        {
                            this.isAccountComplete(this.props.data.user) && <Alert message="Please add a card and bank account before accepting requests" type="error" />
                        }
                        <br />
                        <Button loading={this.state.loading} disabled={this.isAccountComplete(this.props.data.user)} onClick={this.handleAcceptRequest} icon="like" size="large" type="primary">Accept</Button>
                    </Card>
                </Card>
            </div>
        )
    }
}

const Accept = graphql(AcceptRequestQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserAccepts)

export { Accept }