import React, { Component } from 'react'
import { Input, Button, Alert, message } from 'antd'
import { isEmpty } from 'lodash'
import { graphql } from 'react-apollo'
import { getUser } from '../../../../../utils'
import { UpdateTransactionMutation } from '../../../../../mutations'

class UserBankAccount extends Component {
    constructor(props) {
        super(props)
        this.state = {
            country: 'CA',
            currency: 'cad',
            routing_number: '',
            account_number: '',
            account_holder_name: '',
            account_holder_type: 'individual',
            loading: false
        }
        this.createBankAccountToken = this.createBankAccountToken.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    createBankAccountToken() {
        const { firstName, lastName } = this.props.data.user
        this.setState({ loading: true })
        this.setState({ account_holder_name: `${firstName} ${lastName}` }, () => {
            const { country, currency, routing_number, account_number, account_holder_name, account_holder_type } = this.state
            this.props.stripe.createToken('bank_account', { country, currency, routing_number, account_number, account_holder_name, account_holder_type }).then((result) => {
                // Handle result.error or result.token
                if (!isEmpty(result.error)) {
                    this.setState({ loading: false }, () => {
                        message.error(result.error.message)
                    })
                }
                if (!isEmpty(result.token)) {
                    const args = { id: getUser().userId, bankAccountToken: result.token.id, clientIp: result.token.client_ip, created: result.token.created.toString()  }
                    this.props.mutate({ variables: args }).then((res) => {
                        this.setState({ loading: false }, () => {
                            message.success("Bank account was successfully added")
                        })
                    }).catch(err => {
                        this.setState({ loading: false }, () => {
                            message.error("Something went wrong with our servers. Please Try again")
                        })
                        throw err
                    })
                }
            })
        })
    }

    render() {
        const user = !isEmpty(this.props.data.user) ? this.props.data.user : {}
        return (
            <div className="container-fluid">
                <br />
                <div className="form-group">
                    <label>Full Name </label>
                    <Input onChange={this.handleChange} disabled={true} name="account_holder_name" defaultValue={`${user.firstName} ${user.lastName}`} />
                </div>
                <div className="form-group">
                    <label>Account Number </label>
                    <Input onChange={this.handleChange} name="account_number" placeholder="**** **** **** ****" />
                </div>
                <div className="form-group">
                    <Alert message="Routing number is a combination of your bank's financial institution and transit numbers" type="info" />
                    <br />
                    <label>Routing Number </label>
                    <Input onChange={this.handleChange} name="routing_number" placeholder="**** **** ****" />
                </div>
                <div style={{ textAlign: 'center' }}>
                    <Button onClick={this.createBankAccountToken} size="large" type="primary" loading={this.state.loading} icon="plus">
                        Add
                    </Button>
                </div>
            </div>
        )
    }
}

const BankAccount = graphql(UpdateTransactionMutation)(UserBankAccount)

export { BankAccount }