import React, { Component } from 'react'
import { isEmpty } from 'lodash'
import { graphql } from 'react-apollo'
import { Button } from 'antd'
import { getUser } from '../../../../../utils'
import { TransactionQuery } from '../../../../../queries'
import { UpdateTransactionMutation } from '../../../../../mutations'
import {
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement,
    PostalCodeElement,
    Elements,
    injectStripe,
} from 'react-stripe-elements'
import { message } from 'antd'

const createOptions = (fontSize, padding) => {
    return {
        style: {
            base: {
                fontSize,
                color: '#424770',
                letterSpacing: '0.025em',
                fontFamily: 'Source Code Pro, monospace',
                '::placeholder': {
                    color: '#aab7c4',
                },
                ...(padding ? { padding } : {}),
            },
            invalid: {
                color: '#9e2146',
            },
        },
    }
}

class _SplitForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit = (ev) => {
        this.setState({ loading: true })
        ev.preventDefault()
        if (this.props.stripe) {
            this.props.stripe
                .createToken({ currency: 'usd' })
                .then((payload) => {
                    console.log('[token]', payload)
                    if (!isEmpty(payload.error)) {
                        this.setState({ loading: false }, () => {
                            message.error(payload.error.message)
                        })
                    } else {
                        if (!isEmpty(payload.token)) {
                            const args = { id: getUser().userId, cardToken: payload.token.id, clientIp: payload.token.client_ip, created: payload.token.created.toString() }
                            this.props.mutate({ variables: args }).then(() => {
                                this.props.data.refetch().then(() => {
                                    this.setState({ loading: false }, () => {
                                        message.success('card was successfully added')
                                    })
                                })
                            })
                        }
                    }
                })
        } else {
            console.log("Stripe.js hasn't loaded yet.")
        }
    }

    handleBlur = () => {
        console.log('[blur]')
    }
    handleChange = (change) => {
        console.log('[change]', change)
    }
    handleClick = () => {
        console.log('[click]')
    }
    handleFocus = () => {
        console.log('[focus]')
    }
    handleReady = () => {
        console.log('[ready]')
    }

    render() {
        // const { loading } = this.state
        return (
            <form className="stripe-form" onSubmit={this.handleSubmit}>
                <label className="stripe-label">
                    Card number
            <CardNumberElement
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        onReady={this.handleReady}
                        {...createOptions(this.props.fontSize)}
                    />
                </label>
                <br />
                <label className="stripe-label">
                    Expiration date
            <CardExpiryElement
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        onReady={this.handleReady}
                        {...createOptions(this.props.fontSize)}
                    />
                </label>
                <br />
                <label className="stripe-label">
                    CVC
            <CardCVCElement
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        onReady={this.handleReady}
                        {...createOptions(this.props.fontSize)}
                    />
                </label>
                <br />
                <label className="stripe-label">
                    Postal code
            <PostalCodeElement
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                        onFocus={this.handleFocus}
                        onReady={this.handleReady}
                        {...createOptions(this.props.fontSize)}
                    />
                </label>
                <br />
                {/* <button className="stripe-button">Add</button> */}
                <div style={{ textAlign: 'center' }}>
                    <Button onClick={this.handleSubmit} size="large" type="primary" loading={this.state.loading} icon="plus">
                        Add
                    </Button>
                </div>
            </form>
        )
    }
}

const PreSplitForm = graphql(UpdateTransactionMutation)(
    graphql(TransactionQuery, {
        options: () => { return { variables: { id: getUser().userId } } }
    })(injectStripe(_SplitForm))
)

const SplitForm = PreSplitForm

class Checkout extends Component {
    constructor() {
        super();
        this.state = {
            elementFontSize: window.innerWidth < 450 ? '14px' : '18px',
        };
        window.addEventListener('resize', () => {
            if (window.innerWidth < 450 && this.state.elementFontSize !== '14px') {
                this.setState({ elementFontSize: '14px' });
            } else if (
                window.innerWidth >= 450 &&
                this.state.elementFontSize !== '18px'
            ) {
                this.setState({ elementFontSize: '18px' });
            }
        });
    }

    render() {
        const { elementFontSize } = this.state;
        return (
            <div className="Checkout">
                <Elements>
                    <SplitForm fontSize={elementFontSize} />
                </Elements>
            </div>
        );
    }
}

export { Checkout }