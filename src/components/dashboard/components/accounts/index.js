import React, { Component } from 'react'
import { StripeProvider, Elements, injectStripe } from 'react-stripe-elements'
import { graphql } from 'react-apollo'
import Cards from 'react-credit-cards'
import { Card, Alert } from 'antd'
import 'react-credit-cards/es/styles-compiled.css'
import { TransactionQuery } from '../../../../queries'
import { getUser } from '../../../../utils'
import { Checkout, BankAccount } from './components'
import { MandatoryFields } from '../../common'

const AddBankAccount = injectStripe(BankAccount)

class UserAccount extends Component {
    constructor(props) {
        super(props)
        this.state = {
            stripe: null
        }
    }

    componentDidMount() {
        this.props.data.refetch()
        if (window.Stripe) {
            this.setState({ stripe: window.Stripe('pk_test_xv6RdQ1hTgzKv3yl8l2669sW') })
        } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
                // Create Stripe instance once Stripe.js loads
                this.setState({ stripe: window.Stripe('pk_test_xv6RdQ1hTgzKv3yl8l2669sW') })
            })
        }
    }

    displayPrimaryCards({ user }) {
        let result = <div>No primary card has been added yet...</div>
        if (user) {
            const { firstName, lastName, transaction } = user
            if (transaction.cardTokens) {
                transaction.cardTokens.forEach(card => {
                    let { exp_month, exp_year, last4 } = card.cardToken.card
                    exp_month = (exp_month.toString().length <= 1) ? `0${exp_month}` : exp_month
                    let num = `**** **** **** ....`
                    num = num.replace('....', last4)
                    result = <Cards
                        preview={true}
                        issuer={"visa"}
                        number={num}
                        name={`${firstName} ${lastName}`}
                        expiry={`${exp_month}/${exp_year}`}
                        cvc={`***`}
                        locale={{ valid: '' }}
                        focused={"number"}
                    />
                })
            }
        }
        return result
    }

    render() {
        return (
            <div style={{ width: '100%' }}>
                <div style={{ width: '100%' }} className="card-columns">
                    <Card style={{ width: '100%' }} className="card" title="Add Bank Account" bordered={true} loading={this.props.data.loading}>
                        <Alert message="Bank accounts are preferable for quicker and faster transactions" type="info" />
                        <StripeProvider stripe={this.state.stripe}>
                            <Elements>
                                <AddBankAccount {...this.props} />
                            </Elements>
                        </StripeProvider>
                    </Card>
                <Card style={{ width: '100%' }} className="card" title="Primary Card(s)" bordered={true} loading={this.props.data.loading}>
                    {this.displayPrimaryCards(this.props.data)}
                </Card>
                <Card style={{ width: '100%' }} className="card" title="Add Card" bordered={true} loading={this.props.data.loading}>
                    <StripeProvider apiKey="pk_test_xv6RdQ1hTgzKv3yl8l2669sW">
                        <Checkout />
                    </StripeProvider>
                </Card>
                <MandatoryFields />
            </div>
            </div >
        )
    }
}

const Accounts = graphql(TransactionQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserAccount)

export { Accounts } 