import React, { Component } from 'react'
import Img from 'react-image'
import firebase from 'firebase/app'
import 'firebase/storage'
import { graphql } from 'react-apollo'
import { currentUser } from '../../../../queries'
import { UpdateUserMutation } from '../../../../mutations'
import { getUser, getFirebaseConfig } from '../../../../utils'
import { MandatoryFields } from '../../common'
import { pick, merge } from 'lodash'
import { Card, Icon, Button, Select, Input, message, Progress } from 'antd'

const Option = Select.Option;

class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
            buttonLoading: false,
            uploadPercent: 0
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleUpdate = this.handleUpdate.bind(this)
        this.handleSelect = this.handleSelect.bind(this)
        this.handleUpload = this.handleUpload.bind(this)
    }

    componentDidMount() {
        this.state.data.refetch()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ data: nextProps.data })
    }

    handleUpload(e) {
        if (!firebase.apps.length) {
            firebase.initializeApp(getFirebaseConfig())
        }
        const storage = firebase.storage()
        let file = e.target.files[0]
        let storageRef = storage.ref(`/avatars/${getUser().userId}.png`)
        let task = storageRef.put(file)
        task.on('state_changed', snapshot => {
            const percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            const progress_bar = percentage
            this.setState({ uploadPercent: Number(progress_bar) })
        }, err => { throw err }, () => {
            task.snapshot.ref.getDownloadURL().then((downloadURL) => {
                this.setState({ avatar: downloadURL }, () => {
                    const args = this.buildArgs(this.state)
                    this.props.mutate({ variables: args }).then(() => {
                        this.props.data.refetch().then(() => {
                            this.setState({ uploadPercent: 0 }, () => {
                                message.info('avatar was successfully updated')
                            })
                        })
                    }).catch(err => { throw err })
                })
            })
        })
    }


    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSelect(value) {
        this.setState({ accountType: value })
    }

    handleUpdate() {
        this.setState({ buttonLoading: true })
        const args = this.buildArgs(this.state)
        this.props.mutate({ variables: args })
            .then(() => {
                this.props.data.refetch().then(() => {
                    message.info('your account was successfully updated')
                    this.setState({ buttonLoading: false })
                })
            }).catch(err => { throw err })
    }

    buildArgs(args) {
        return merge(pick(args, ['email', 'accountType', 'age', 'phoneNumber', 'userName', 'avatar']), { id: getUser().userId })
    }

    isdisabled = (user, field) => {
        if (user) {
            return (user[field] === 'empty') ? false : true
        }
        return false
    }

    renderVerificationButton = (user) => {
        if (user) {
            return (user.verified) ? (
                <Button icon="check-circle" style={{ backgroundColor: '#70C040' }} block disabled={true}>
                    Verified
                </Button>) : (<Button icon="exclamation-circle" type="primary" block>
                    Verify
                </Button>)
        }
    }

    render() {
        let { loading, user } = this.state.data
        return (
            <div className="card-columns">
                <Card className="card" title="Account Info" loading={loading}>
                    <div className="container-fluid">
                        <div className="form-group">
                            <label>Email Address </label>
                            <Input name="email" onChange={this.handleChange} defaultValue={(user) ? user.email : ''} placeholder="Email" />
                        </div>
                        <div className="form-group">
                            <label>UserName </label>
                            <Input name="userName" onChange={this.handleChange} placeholder="UserName" defaultValue={(user) ? user.userName : ''} disabled={true} />
                        </div>
                        <div className="form-group">
                            <label>Age </label>
                            <Input name="age" onChange={this.handleChange} defaultValue={(user) ? user.age : ''} placeholder="Age" disabled={this.isdisabled(user, 'age')} />
                        </div>
                        <div className="form-group">
                            <label>AccountType </label>
                            <Select name="accountType" defaultValue={(user) ? user.accountType : ''} style={{ width: '100%' }} onChange={this.handleSelect}>
                                <Option value="borrower">Borrower</Option>
                                <Option value="loaner">Loaner</Option>
                            </Select>
                        </div>
                        <div className="form-group">
                            <label>Phone </label>
                            <Input name="phoneNumber" onChange={this.handleChange} defaultValue={(user) ? user.phoneNumber : ''} placeholder="Phone" disabled={this.isdisabled(user, 'phoneNumber')} />
                        </div>
                        <div style={{ textAlign: 'center' }}>
                            <Button size="large" type="primary" icon="up-square" loading={this.state.buttonLoading} onClick={this.handleUpdate}>
                                Update
                            </Button>
                        </div>
                    </div>
                </Card>
                <Card className="card" title="Verified" loading={loading}>
                    {this.renderVerificationButton(user)}
                </Card>
                <Card className="card" loading={loading} style={{ textAlign: 'center' }}>
                    <Img className="card-img" src={(user) ? user.avatar : ''} alt="avatar" />
                    <hr />
                    <br />
                    <label className="btn btn-sm btn-primary btn-block">
                        <Icon type="upload" style={{ marginRight: '5px' }} />
                        Change Avatar <input type="file" hidden onChange={this.handleUpload} />
                    </label>
                    <Progress percent={this.state.uploadPercent} status="active" showInfo={false} />
                </Card>
                <MandatoryFields {...this.props}/>
            </div>
        )
    }
}

const UpdateProfile = graphql(currentUser, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserProfile)

const Profile = graphql(UpdateUserMutation)(UpdateProfile)

export { Profile }