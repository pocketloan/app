import React, { Component } from 'react'
import { Button, Card } from 'antd'

class HelpAndSupport extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="text-center">
                    <Button disabled={true} type="primary" icon="usergroup-add" size="large">Help & Support</Button>
                    <br />
                    <br />
                </div>
                <div className="card-columns">
                    <Card className="card" title="Contact Us" bordered={true}>
                        <div> Coming Soon... </div>
                    </Card>
                    <Card className="card" title="FAQ's" bordered={true}>
                        <div> Coming Soon... </div>
                    </Card>
                    <Card className="card" title="Live Chat" bordered={true}>
                        <div> Coming Soon... </div>
                    </Card>
                    <Card className="card" title="Tips" bordered={true}>
                        <div> Coming Soon... </div>
                    </Card>
                    <Card className="card" title="Getting Started" bordered={true}>
                        <div> Coming Soon... </div>
                    </Card>
                </div>
            </div>
        )
    }
}

export { HelpAndSupport }