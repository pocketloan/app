import React, { Component } from 'react'
import { DashboardLayout } from '../layout'
import { Home, Profile, Inbox, TransactionHistory, Accounts, Settings, HelpAndSupport, Accept, Request } from './components'
import { graphql } from 'react-apollo'
import { getUser } from '../../utils'
import { currentUser } from '../../queries'

const DashboardComponents = { Home, Profile, Inbox, TransactionHistory, Accounts, Settings, HelpAndSupport, Accept, Request }

class Board extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return <DashboardLayout {...this.props} components={DashboardComponents} />
    }
}

const Dashboard = graphql(currentUser, {
    options: () => { return { variables: { id: getUser().userId } } }
})(Board)

export { Dashboard }