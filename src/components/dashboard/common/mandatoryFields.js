import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import { isEmpty } from 'lodash'
import { getUser } from '../../../utils'
import { MandatoryFieldsQuery } from '../../../queries'
import { UpdateMandatoryFields } from '../../../mutations'
import { Card, Input, Button, Alert, message, Select } from 'antd'
const Option = Select.Option;

class UserMandatoryFields extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            sin: "",
            dob: "",
            address: "",
            city: "",
            state: "ON",
            postalCode: "",
            loading: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.selected = this.selected.bind(this)
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    selected(state) {
        this.setState({ state })
    }

    componentWillReceiveProps(nextProps) {
        if (!isEmpty(nextProps.data.user)) {
            this.setState({ user: nextProps.data.user })
        }
    }

    componentDidMount() {
        this.props.data.refetch()
    }

    dateOfBirth(account) {
        let result = 'dd/mm/yyyy'
        if (!isEmpty(account)) {
            const dob = account.legal_entity.dob
            if (dob.day && dob.month && dob.year) {
                result = `${dob.day}/${dob.month}/${dob.year}`
            }
        }
        return result
    }

    displaySocialInsuranceNumber(user) {
        let result = '**** **** ****'
        if (!isEmpty(user)) {
            if (!isEmpty(user.socialInsuranceNumber)) {
                result = user.socialInsuranceNumber
            }
        }
        return result
    }

    handleSubmit() {
        this.setState({ loading: true })
        if (this.validateFields(this.state)) {
            const { user, dob, sin, city, state, address, postalCode } = this.state
            if (sin.length < 9 || sin.length > 9) {
                this.setState({ loading: false }, () => {
                    message.error(`Social insurance number is invalid`)
                })
                return
            }
            const args = {
                city,
                state,
                address,
                postalCode,
                userId: user._id,
                dob,
                socialInsuranceNumber: sin,
                accountId: user.transaction.accounts[0].account.id
            }
            this.props.mutate({ variables: args }).then(() => {
                this.setState({ loading: false }, () => {
                    message.success(`mandatory fields were successfully updated`)
                })
            }).catch(err => {
                throw err
            })
        } else {
            this.setState({ loading: false })
        }
    }

    validateFields({ dob, sin, state, city, address, postalCode }) {
        let result = false
        if (!isEmpty(dob) && !isEmpty(sin) && !isEmpty(city) && !isEmpty(state) && !isEmpty(address) && !isEmpty(postalCode)) {
            const validateDob = dob.split('/')
            if (validateDob.length === 3 && /^[0-9]*$/.test(sin)) {
                result = true
            }
        }
        if (!result) {
            message.error(`Form field(s) are either empty or invalid`)
        }
        return result
    }

    render() {
        const { user } = this.state
        const accounts = (!isEmpty(user.transaction)) ? user.transaction.accounts : {}
        const account = (!isEmpty(accounts)) ? accounts[0].account : {}
        const legal_entity = (!isEmpty(account.legal_entity)) ? account.legal_entity : {}
        return (
            <div>
                <Card className="card" title="Mandatory Fields" loading={this.props.data.loading}>
                    <Alert message="You need to fill these fields to be able to perform transactions on the platform" type="info" />
                    <br />
                    <Alert message="You also need to add a credit or debit card or preferably bank account" type="warning" />
                    <br />
                    <div className="container-fluid">
                        <div className="form-group">
                            <label>Social Insurance Number</label>
                            <Input onChange={this.handleChange} name="sin" placeholder={`${this.displaySocialInsuranceNumber(user)}`} />
                        </div>
                        <div className="form-group">
                            <label>Date Of Birth </label>
                            <Input onChange={this.handleChange} name="dob" placeholder={`${this.dateOfBirth(account)}`} />
                        </div>
                        <div className="form-group">
                            <label>Address </label>
                            <Input onChange={this.handleChange} name="address" placeholder={`${!isEmpty(legal_entity.address) ? legal_entity.address.line1 : 'none'}`} />
                        </div>
                        <div className="form-group">
                            <label>Province </label>
                            <Select defaultValue={`${!isEmpty(legal_entity.address) ? legal_entity.address.state : 'ON'}`} style={{ width: '100%' }} onChange={this.selected}>
                                <Option value="AB">AB</Option>
                                <Option value="BC">BC</Option>
                                <Option value="MB">MB</Option>
                                <Option value="NB">NB</Option>
                                <Option value="NL">NL</Option>
                                <Option value="NS">NS</Option>
                                <Option value="NT">NT</Option>
                                <Option value="NU">NU</Option>
                                <Option value="ON">ON</Option>
                                <Option value="PE">PE</Option>
                                <Option value="QC">QC</Option>
                                <Option value="SK">SK</Option>
                                <Option value="YT">YT</Option>
                            </Select>
                        </div>
                        <div className="form-group">
                            <label>City </label>
                            <Input onChange={this.handleChange} name="city" placeholder={`${!isEmpty(legal_entity.address) ? legal_entity.address.city : 'none'}`} />
                        </div>
                        <div className="form-group">
                            <label>PostalCode </label>
                            <Input onChange={this.handleChange} name="postalCode" placeholder={`${!isEmpty(legal_entity.address) ? legal_entity.address.postal_code : 'none'}`} />
                        </div>
                        <div style={{ textAlign: 'center' }}>
                            <Button onClick={this.handleSubmit} size="large" type="primary" icon="up-square" loading={this.state.loading}>
                                Update
                            </Button>
                        </div>
                    </div>
                </Card>
            </div>)
    }
}

const MutateMandatoryFields = graphql(MandatoryFieldsQuery, {
    options: () => { return { variables: { id: getUser().userId } } }
})(UserMandatoryFields)

const MandatoryFields = graphql(UpdateMandatoryFields)(MutateMandatoryFields)

export { MandatoryFields }