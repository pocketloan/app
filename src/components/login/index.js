import React, { Component } from 'react'
import { FormLayout } from '../layout'
import { Form, Icon, Input, Button, Checkbox, message } from 'antd'
import { Link } from 'react-router-dom'
import { graphql } from 'react-apollo'
import { css } from 'emotion'
import { LoginMutation } from '../../mutations'

const FormItem = Form.Item

const loginForm = css`
    max-width: 52%;
    text-align: center;
`

const position = css`
    text-align: center;
    margin-top: 8%
`

const forgot = css`
    float: right;
`

const button = css`
    width: 100%;
`
const mobileWidth = css`
    width: 200%;
    text-align: center;
    margin-right: 5%;
    margin-top: 30%;
`
const normalWidth = css`
    width: 50vw;
    text-align: center;
    margin-left: 36%;
`

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.renderLogin = this.renderLogin.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            isLoading: false
        }
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (values.userName && values.password) {
                const variables = this.buildLogin(values)
                if (!err) {
                    this.setState(values)
                    this.setState({ isLoading: true })
                    this.props.mutate({
                        variables
                    }).then(res => {
                        this.setState({ isLoading: false })
                        const { status } = res.data.login
                        if (status !== 200) {
                            message.error(res.data.login.message)
                        } else {
                            const { accessToken } = res.data.login
                            localStorage.setItem('accessToken', accessToken)
                            this.props.history.push('/dashboard')
                        }
                    }).catch(err => { throw err })
                }
            }
        })
    }

    buildLogin = (values) => {
        const { userName, password } = values
        return { field: userName.toLowerCase(), password }
    }

    renderButton() {
        const { isLoading } = this.state
        return (!isLoading)
            ? <Button type="primary" htmlType="submit" className={button}> Log in </Button>
            : <Button type="primary" className={button} loading> Loading </Button>
    }

    renderLogin() {
        const { getFieldDecorator } = this.props.form
        return (
            <div className={`${position} ${window.innerWidth <= 450 ? mobileWidth : normalWidth}`}>
                <Form onSubmit={this.handleSubmit} className={loginForm}>
                    <FormItem>
                        {getFieldDecorator('userName', {
                            rules: [{ required: true, message: 'Please input your Email or Username!' }],
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>Remember me</Checkbox>
                        )}
                        <Link to={`/forgot`} className={forgot} >Forgot password</Link>
                        <br />
                        {this.renderButton()}
                        Or <Link to={`/register`}>Register Now!</Link>
                    </FormItem>
                </Form>
            </div>
        )
    }

    render() {
        return <FormLayout content={this.renderLogin} />
    }
}

const SignUp = Form.create()(LoginForm)
const Login = graphql(LoginMutation)(SignUp)

export { Login }
