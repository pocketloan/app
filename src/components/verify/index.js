import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import { isEmpty } from 'lodash'
import { Alert, message } from 'antd'
import { FormLayout } from '../layout'
import { VerifyMutation } from '../../mutations'

class VerifyUser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            response: {}
        }
        this.renderVerify = this.renderVerify.bind(this)
        this.handleInvalid = this.handleInvalid.bind(this)
    }

    componentDidMount() {
        const { id } = this.state
        this.props.mutate({ variables: { id } }).then(res => {
            const response = res.data.verifyUser
            this.handleInvalid(response)
            this.setState({ response })
        }).catch(err => { throw err })
    }

    handleInvalid(response) {
        if (response.status.toString() !== '200' && isEmpty(response.user)) {
            message.error(`error! no user exists with that id`)
            setTimeout(() => {
                this.props.history.push('/dashboard')
            }, 1000)
        } else {
            message.success(`success! your account is now verified`)
            setTimeout(() => {
                this.props.history.push('/dashboard')
            }, 2000)
        }
    }

    renderVerify() {
        const { message, user } = this.state.response
        return (
            <div style={{ fontWeight: 'bold', textAlign: 'center', marginTop: '5%' }}>
                <Alert message={(message) ? message : ''} type={user ? 'success' : 'error'} />
            </div>
        )
    }

    render() {
        return <FormLayout content={this.renderVerify} />
    }
}

const Verify = graphql(VerifyMutation)(VerifyUser)

export { Verify }
