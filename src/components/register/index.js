import React, { Component } from 'react'
import { Steps, Button, message } from 'antd'
import { graphql } from 'react-apollo'
import { css } from 'emotion'
import { pick, merge } from 'lodash'
import { Link } from 'react-router-dom'
import { FormLayout } from '../layout'
import { BasicInfo, AccountType, TermsAndConditions } from './components'
import { accountTypeStore, basicInfoStore, termsAndConditionStore } from '../../store'
import { validate } from '../../utils'
import { RegisterMutation } from '../../mutations'

const Step = Steps.Step;

const steps = [{
    title: 'Basic Info',
    content: <BasicInfo />,
    store: basicInfoStore
}, {
    title: 'Account Type',
    content: <AccountType />,
    store: accountTypeStore
}, {
    title: 'Terms And Conditions',
    content: <TermsAndConditions />,
    store: termsAndConditionStore
}]

const textAlign = css`
    text-align: center;
`

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            isLoading: false
        }
        this.renderRegister = this.renderRegister.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit() {
        const variables = this.buildUserInfo()
        return this.props.mutate({
            variables
        }).then(res => {
            return res.data.createUser
        }).catch(err => { throw err })
    }

    displayMessage({ status, accessToken }) {
        const { firstName } = steps[0].store.getState()
        if (status === 200) {
            localStorage.setItem('accessToken', accessToken)
            message.success(`welcome to pocketloan ${firstName}`)
            this.props.history.push('/dashboard')
        }
    }

    buildUserInfo() {
        const { result } = pick(accountTypeStore.getState(), ['result'])
        const userInfo = pick(basicInfoStore.getState(), ['firstName', 'lastName', 'email', 'password', 'sex'])
        let user = merge({ accountType: result }, userInfo)
        user.userName = `${user.firstName}${user.lastName}`.toLowerCase()
        user.email = user.email.toLowerCase()
        return user
    }


    next() {
        let { isValid, errorMessage } = validate(steps[this.state.current])
        if (isValid) {
            const current = this.state.current + 1;
            this.setState({ current })
        } else {
            errorMessage = errorMessage.replace('sex', 'gender')
            message.error(errorMessage)
        }
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current })
    }

    async done() {
        this.setState({ isLoading: true })
        let { isValid, errorMessage } = validate(steps[this.state.current])
        if (isValid) {
            const serverResponse = await this.onSubmit()
            this.displayMessage(serverResponse)
        } else {
            message.error(errorMessage)
        }

    }

    renderDoneButton(isLoading) {
        return (!isLoading)
            ? <Button style={{ marginLeft: 8 }} type="primary" onClick={() => this.done()}>Done</Button>
            : <Button style={{ marginLeft: 8 }} type="primary" loading> Loading </Button>
    }

    renderRegister() {
        const { current, isLoading } = this.state
        return (<div>
            <Steps current={current}>
                {steps.map(item => <Step key={item.title} title={item.title} />)}
            </Steps>
            <div className={`steps-content ${textAlign}`}>{steps[current].content}</div>
            <div className={`steps-action ${textAlign}`}>
                {
                    current > 0
                    && (
                        <Button onClick={() => this.prev()}>
                            Previous
                        </Button>
                    )
                }
                {
                    current === steps.length - 1
                    && this.renderDoneButton(isLoading)
                }
                {
                    current < steps.length - 1
                    && <Button style={{ marginLeft: 8 }} type="primary" onClick={() => this.next()}>Next</Button>
                }
            </div>
            <br />
            <div className={textAlign}>
                <p>
                    <b>
                        Already have an account? <Link to={`/login`}>Login</Link>
                    </b>
                </p>
            </div>
        </div>)
    }

    render() {
        return <FormLayout content={this.renderRegister} />
    }
}


const Register = graphql(RegisterMutation)(SignUp)

export { Register }