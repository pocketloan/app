import React, { Component } from 'react'
import { Checkbox } from 'antd'
import { css } from 'emotion'
import { Link } from 'react-router-dom'
import { termsAndConditionStore } from '../../../store'

const position = css`
    width: 100%,
    text-align: center;
    margin-top: 1%;
    margin-bottom: 1%;
`


class TermsAndConditions extends Component {
    constructor(props) {
        super(props)
        this.state = termsAndConditionStore.getState()
    }
    onChange = (e) => {
        this.setState({ checked: e.target.checked }, () => {
            termsAndConditionStore.setState(this.state)
        })
    }

    componentDidMount() {
        const currentState = termsAndConditionStore.getState()
        this.setState(currentState)
    }

    render() {
        const { checked } = this.state
        return (
            <div className={position}>
                <br />
                <h3>
                    PocketLoan <Link to={`/terms`}>Terms And Conditions</Link>
                </h3>
                <Checkbox checked={checked} onChange={this.onChange}>
                    <b> I agree to the PocketLoan Terms and Conditions cited above </b>
                </Checkbox>
                <br />
                <br />
            </div>
        )
    }
}

export { TermsAndConditions }