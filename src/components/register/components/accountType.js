import React, { Component } from 'react'
import { Card, Button } from 'antd'
import { css } from 'emotion'
import { accountTypeStore } from '../../../store'

const position = css`
    text-align: center;
    margin-top: 1%;
    margin-bottom: 1%;
`

const card = css`
    text-align: center;
    width: 100%;
`

const check = css`
    display: inline;
    text-align: center;
`
class AccountType extends Component {
    constructor(props) {
        super(props)
        this.state = accountTypeStore.getState()
        this.clickBorrower = this.clickBorrower.bind(this)
        this.clickLoaner = this.clickLoaner.bind(this)
    }

    clickBorrower() {
        const state = {
            loanerType: 'default',
            borrowerType: 'primary',
            result: 'borrower'
        }
        this.setState(state)
        accountTypeStore.setState(state)
    }

    clickLoaner() {
        const state = {
            loanerType: 'primary',
            borrowerType: 'default',
            result: 'loaner'
        }
        this.setState(state)
        accountTypeStore.setState(state)
    }

    componentDidMount() {
        const currentState = accountTypeStore.getState()
        this.setState(currentState)
    }

    render() {
        const { borrowerType, loanerType } = this.state
        return (
            <div className={position}>
                <Card title="Do you want to be a lender?" bordered={false} className={card}>
                    <p>
                        <b>PocketLoan provides you the tools in other to manage your transactions and also be in control of your own intrest rates</b>
                    </p>
                    <div className={check}>
                        <Button type={borrowerType} size='large' onClick={this.clickBorrower}>Borrower</Button>
                        <Button style={{ marginLeft: '5px', marginTop: '20px' }} type={loanerType} size='large' onClick={this.clickLoaner}>Loaner</Button>
                    </div>
                </Card>
            </div>
        )
    }
}

export { AccountType }