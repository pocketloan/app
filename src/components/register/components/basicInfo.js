import React, { Component } from 'react'
import { Form, Input, Select } from 'antd'
import { isEmpty } from 'lodash'
import { css } from 'emotion'
import { basicInfoStore } from '../../../store'

const form = css`
    margin-top: 1%
`
const Option = Select.Option
const FormItem = Form.Item

const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
}

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = basicInfoStore.getState()
        this.handleChange = this.handleChange.bind(this)
    }

    getInitialState() {
        return this.state
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.setStore(this.state)
        })
    }

    setStore = (state) => {
        const store = state
        basicInfoStore.setState(store)
    }

    selected = (sex) => {
        this.setState({ sex })
    }

    check = () => {
        this.props.form.validateFields(
            (err) => {
                if (!err) {
                    console.info('success');
                }
            },
        );
    }

    default = (field) => {
        return (!isEmpty(field)) ? field : null
    }

    componentDidMount() {
        const currentState = basicInfoStore.getState()
        this.setState(currentState)
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { firstName, lastName, email, sex, password, confirmPassword } = basicInfoStore.getState()
        return (
            <div className={form}>
                <FormItem {...formItemLayout} label="FirstName">
                    {getFieldDecorator('firstName', {
                        initialValue: (!isEmpty(firstName)) ? firstName : null,
                        rules: [{
                            required: true,
                            message: 'Please input your first name',
                        }],
                    })(
                        <Input onChange={this.handleChange} name="firstName" placeholder="Please input your first name" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="LastName">
                    {getFieldDecorator('lastName', {
                        initialValue: (!isEmpty(lastName)) ? lastName : null,
                        rules: [{
                            required: true,
                            message: 'Please input your last name',
                        }],
                    })(
                        <Input onChange={this.handleChange} name="lastName" placeholder="Please input your last name" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="Email">
                    {getFieldDecorator('email', {
                        initialValue: (!isEmpty(email)) ? email : null,
                        rules: [{
                            required: true,
                            message: 'Please input your email address',
                        }],
                    })(
                        <Input onChange={this.handleChange} name="email" placeholder="Please input your email address" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="Sex">
                    {getFieldDecorator('gender', {
                        initialValue: (!isEmpty(sex)) ? sex : 'Other',
                        rules: [{
                            required: true,
                            message: 'Please select one',
                        }],
                    })(<Select placeholder="Please select one" style={{ width: '100%' }} onChange={this.selected}>
                        <Option value="Male">Male</Option>
                        <Option value="Female">Female</Option>
                        <Option value="Other">Other</Option>
                    </Select>)}
                </FormItem>
                <FormItem {...formItemLayout} label="Password">
                    {getFieldDecorator('password', {
                        initialValue: (!isEmpty(password)) ? password : null,
                        rules: [{
                            required: true,
                            message: 'Please input your passowrd',
                        }],
                    })(
                        <Input onChange={this.handleChange} name="password" type="password" placeholder="Please input your passowrd" />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="Confirm Password">
                    {getFieldDecorator('confrimPassword', {
                        initialValue: (!isEmpty(confirmPassword)) ? confirmPassword : null,
                        rules: [{
                            required: true,
                            message: 'Please confirm your password',
                        }],
                    })(
                        <Input onChange={this.handleChange} name="confirmPassword" type="password" placeholder="Please confirm your password" />
                    )}
                </FormItem>
            </div>
        )
    }
}

const BasicInfo = Form.create()(Register)
export { BasicInfo }