import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { getUser } from '../../utils'

const isAuthenticated = () => {
    let isAuth = false
    const decoded = getUser()
    if (decoded.exp) {
        const currentTime = Date.now() / 1000
        isAuth = (decoded.exp < currentTime) ? invalidateUser() : true
    }
    return isAuth
}

const invalidateUser = () => {
    localStorage.removeItem('accessToken')
    localStorage.removeItem('isNotified')
    return false
}

const PublicRoute = ({ ...props }) => {
    const isAllowed = isAuthenticated()
    return isAllowed
        ? (<Redirect to="/dashboard" />)
        : (<Route {...props} />)
}

const ProtectedRoute = ({ ...props }) => {
    const isAllowed = isAuthenticated()
    return isAllowed
        ? (<Route {...props} />)
        : (<Redirect to="/login" />)
}

export { ProtectedRoute, PublicRoute } 