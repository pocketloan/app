import gql from 'graphql-tag';

const currentUser = gql`
    query currentUser($id: ID!) {
        user(id: $id) { 
           _id
           firstName
           lastName
           userName
           email
           age
           accountType
           phoneNumber
           rating
           avatar
           verified
           sex
           transaction {
               _id
               balance
           }
        }
    }`

const TransactionQuery = gql`
query currentUser($id: ID!) {
    user(id: $id) { 
        firstName
        lastName
        transaction {
            cardTokens {
                cardTokenId
                cardToken
            }
        }
    }
}`

const SettingsQuery = gql`
query currentUser($id: ID!) {
    user(id: $id) { 
        email
        transaction {
            cardTokens {
                cardToken
            }
        }
    }
}`

const MandatoryFieldsQuery = gql`
query currentUser($id: ID!) {
    user(id: $id) {
        _id
        socialInsuranceNumber
        transaction {
            accounts {
                account
            }
        }
    }
}`

const UserActivityQuery = gql`
query currentUser($id: ID!) {
    user(id: $id) {
        activity {
            userActivity {
              title
              content
            }
        }
    }
}`

const TransactionHistoryQuery = gql`
    query TransactionHistory($id: ID!) {
        user(id: $id) {
            activity {
                transactionActivity {
                  title
                  content
                  dateCreated
                }
                dateCreated
            } 
        }
    }`

const RequestsQuery = gql`
    query requestQuery($id: ID!) {
        user(id: $id) {
            activity {
                requests {
                    _id
                    title
                    content
                    userId
                    user {
                        avatar
                    }
                }
            }
        }
    }`

const AcceptRequestQuery = gql`
query acceptRequest($id: ID!) {
    user(id: $id) {
      activity {
        requests {
          _id
          title
          content
          amountRequested
        }
        transaction {
          customerId
          accounts {
            account
          }
          cardTokens {
            cardToken
          }
        }
      }
    }
  }
`

export { currentUser, TransactionQuery, SettingsQuery, MandatoryFieldsQuery, UserActivityQuery, TransactionHistoryQuery, RequestsQuery, AcceptRequestQuery }