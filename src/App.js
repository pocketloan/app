import React, { Component } from 'react'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'
import { isEmpty } from 'lodash'
import { InMemoryCache, defaultDataIdFromObject } from 'apollo-cache-inmemory'
import axios from 'axios'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Dashboard, Login, Register, Verify } from './components'
import { PublicRoute, ProtectedRoute } from './components/auth.js'
import { forceHttps, isDevelopment } from './utils'
import './styles/App.css'

const uri = isDevelopment ? 'http://localhost:3003' : ''
const notificationUrl = isDevelopment ? 'http://localhost:3005' : ''

const cache = new InMemoryCache({
  dataIdFromObject: object => {
    switch (object.__typename) {
      case 'UserType':
        if (object.status !== 200) {
          break
        }
        return object._id
      case 'TransactionType':
        if (object.status !== 200) {
          break
        }
        return object._id
      case 'DefaultResponseType':
        if (object.status !== 200 || isEmpty(object.user)) {
          break
        }
        return object.user._id
      case 'LoginType':
        if (object.status !== 200) {
          break
        }
        return object.user._id
      default:
        return defaultDataIdFromObject(object)
    }
  }
})

const client = new ApolloClient({
  uri,
  onError: (e) => {
    const { networkError, graphQLErrors } = e
    const error = networkError || graphQLErrors
    if (!isEmpty(localStorage.getItem('accessToken')) && error) {
      const context = (graphQLErrors) ? graphQLErrors : networkError
      const message = `An error just occured in pocketloan web application. context: ${context[0].message.toString() || context.toString()}`
      axios(notificationUrl, {
        method: 'POST',
        mode: 'no-cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        data: { message },
        withCredentials: false,
        credentials: 'no-origin',
      }).then(res => res.data)
    }
  },
  cache
})

class App extends Component {
  // componentDidMount() {
  //   forceHttps()
  // }

  componentWillUnmount() {
    this.render()
    this.forceUpdate()
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App">
            <Switch>
              <ProtectedRoute exact path="/" component={Dashboard} />
              <ProtectedRoute path="/dashboard" component={Dashboard} />
              <PublicRoute path="/login" component={Login} />
              <PublicRoute path="/register" component={Register} />
              <Route path="/verify/:id" component={Verify} />
            </Switch>
          </div>
        </Router>
      </ApolloProvider>
    )
  }
}

export default App
